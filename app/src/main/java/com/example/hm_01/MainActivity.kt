package com.example.hm_01

import android.os.Bundle
import androidx.activity.ComponentActivity
import android.view.View
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import com.example.hm_01.ui.theme.Hm_01Theme

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
    fun saludar(v: View) {
        val nombreUser = findViewById<EditText>(R.id.txtNombre)
        val Salud0 = findViewById<TextView>(R.id.lbSaludo)
        if (nombreUser.text.toString()=="") {
            Toast.makeText(this, "Falto capturar un nombre", Toast.LENGTH_SHORT).show()
        } else {
            Salud0.text = "Hola ${nombreUser.text}"
        }
    }

    fun limpiar(v: View) {
        val nombreUser = findViewById<EditText>(R.id.txtNombre)
        val Salud0 = findViewById<TextView>(R.id.lbSaludo)
        if (nombreUser.text.toString()=="" && Salud0.text.toString()=="") {
            Toast.makeText(this, "No hay nada escrito", Toast.LENGTH_SHORT).show()
        } else {
            Salud0.text = ""
            nombreUser.text = null
        }
    }

    fun Fin(v: View) {
        finish()
    }
}

